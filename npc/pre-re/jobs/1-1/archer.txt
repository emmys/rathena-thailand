//===== rAthena Script ======================================= 
//= Archer Job Quest
//===== By: ================================================== 
//= kobra_k88; L0ne_W0lf
//===== Current Version: ===================================== 
//= 1.9
//===== Compatible With: ===================================== 
//= rAthena Project
//===== Description: ========================================= 
//= [Official Conversion]
//= Job quest for Archer classes
//===== Additional Comments: ================================= 
//= 1.0 Fully working.
//= 1.1 Added instant job change for High Novice [Lupus]
//= 1.3 Added Baby Class support [Lupus]
//= 1.4 Optimised and moved first checker to Jfunc1-1 [massdriller]
//= 1.5 Now saves/restores all quest skills [Lupus]
//= 1.6 Merged back JFunc [Lupus]
//= 1.7 Rescripted to Aegis 10.3 stadards. [L0ne_W0lf]
//=     Arrow reward is now equal to the type sof trunks brought.
//=     No longer uses function "F_ToHigh"
//= 1.7a Fixed minor dialogue bug at Grade==25 [Lupus]
//= 1.8 Fixed mistake in condition check. [L0ne_W0lf]
//= 1.9 Added Quest Log commands. [L0ne_W0lf]
//============================================================ 

payon_in02,64,71,4	script	เจ้าหน้าที่สมาคม Archer#archer	85,{
	if (Upper == 1) {
		if (Class == Job_Novice_High && (ADVJOB == Job_Sniper || ADVJOB == Job_Clown || ADVJOB == Job_Gypsy)) {
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " เฮ้, ข้ารู้จักเจ้า";
			mes " เจ้าได้รับบททดสอบ";
			mes " มาก่อนแล้วใช่ไหม?";
			next;
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " อ่า, เจ้าต้องได้ไปที่";
			mes " Valhalla และ ได้รับการจุติ";
			mes " ว้าว, ข้าว่ามันตื่นเต้นน่าดู!";
			next;
			if (!callfunc("F_CanChangeJob")) {
				mes "[เจ้าหน้าที่สมาคม Archer]";
				mes " เออ...";
				mes " เจ้าควรจะเรียนรู้ Basic Skills ทั้งหมด ก่อนที่เจ้าจะมาเป็น Archer ได้";
				next;
				mes "[เจ้าหน้าที่สมาคม Archer]";
				mes " เอาล่ะ, ไว้เจอกันคราวหน้า";
				close;
			}
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " เยี่ยมมม ข้าคงไม่มีอะไรจะสอนเจ้าอีกแล้ว";
			mes " ข้ารู้ว่าเจ้าจะเป็น Archer ที่ดีได้แน่...";
			mes "";
			next;
			skill "NV_TRICKDEAD",0,SKILL_PERM;
			jobchange Job_Archer_high;
			skill "AC_MAKINGARROW",1,SKILL_PERM;
			skill "AC_CHARGEARROW",1,SKILL_PERM;
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " ครั้งนี้อาจไม่มีอะไรพิเศษ";
			mes " ที่ข้าจะมอบให้กับเจ้า แต่ข้าหวังว่าเจ้าคงจะเข้าใจข้านะ ดูแลตัวเองให้ดี";
			close;
		}
		else {
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " โอ้...?";
			mes " เฮ้, นี่อะไร";
			mes " เจ้ามาทำอะไรที่นี่...?";
			next;
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " ข้าสามารถบอกได้ว่าเจ้าไม่ได้ตั้งใจจะเป็น Archer มันรู้สึกว่าเจ้าตั้งใจจะ";
			mes " ทำอย่างอื่น...";
			close;
		}
	}
	mes "[เจ้าหน้าที่สมาคม Archer]";
	mes " หวัดดี มีอะไรให้ช่วยไหม?";
	next;
	switch(select(" • ข้าอยากจะเป็น Archer: • ข้าอยากรู้ข้อกำหนดของ Archer: • ไม่มีอะไร, ขอบคุณ")) {
	case 1:
		if (BaseJob == Job_Archer) {
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " เจ้าได้กลายเป็น Archer แล้ว...";
			close;
		}
		else if (BaseJob != Job_Archer && BaseJob != Job_Novice) {
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " หืมม...";
			mes "เจ้าดูไม่ค่อยเหมือน Novice เลย...";
			next;
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " อย่างไรก็ตามสิ่งที่เจ้าเป็น เจ้าไม่สามารถเลือกอาชีพ Archer ได้เพราะเจ้ามีอาชีพอยู่แล้ว.";
			close;
		}
		if (job_archer_q == 0) {
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " เจ้าต้องการที่จะเป็น Archer อย่างนั้นหรือ?";
			mes " ถ้าอย่างนั้น, เจ้าจำเป็นที่จะต้องกรอกแบบฟอร์มนี้";
			next;
			if (select(" • กรอกแบบฟอร์ม: • ยกเลิก") == 1) {
				set job_archer_q,1;
				setquest 1004;
				mes "[เจ้าหน้าที่สมาคม Archer]";
				mes " โอเค, เซ็นตรงนี้... เอาละ, ข้าจะให้สิ่งที่เจ้าต้องการ";
				next;
				mes "[เจ้าหน้าที่สมาคม Archer]";
				mes " เจ้าคิดว่าพร้อมหรือยัง, เราจะเช็คอีกทีนะ";
				mes " พร้อมหรือยัง?";
				next;
				if (select(" • ข้าพร้อมแล้ว: • ข้ายังไม่พร้อม") == 1) {
					mes "[เจ้าหน้าที่สมาคม Archer]";
					mes " ดีมาก ข้าขอเช็คข้อมูลนะ";
					next;
				}
				else {
					mes "[เจ้าหน้าที่สมาคม Archer]";
					mes " ข้าเข้าใจ เชิญตามสบายถ้าเจ้าต้องการเคล็ดลับข้าบอกเจ้าได้นะ";
					close;
				}
			}
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " อืม, ไม่เป็นไร";
			mes "ไว้เจอกันคราวหน้า";
			close;
		}
		mes "[เจ้าหน้าที่สมาคม Archer]";
		mes " เจ้าคือ..." + strcharinfo(0) + "ใช่ไหม?";
		next;
		if (!callfunc("F_CanChangeJob")) {
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " โอ้, Job level ของเจ้าไม่เหมาะสม โปรดตรวจสอบข้อกำหนดอีกครั้ง";
			next;
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " เจ้าต้องมี Job level อย่างน้อย 10, และ อย่าลืมว่าเจ้าควรเรียนรู้ Basic Skills ทั้งหมด เมื่อเจ้าทำเสร็จแล้วจงกลับมาหาข้า";
			close;
		}
		if (job_archer_q == 1) {
			set .@archer_item1,countitem(1066) * 5;
			set .@archer_item2,countitem(1067) * 3;
			set .@archer_item3,countitem(1068) * 2;
			set .@archer_item4,countitem(1019);
			set .@total_archer,.@archer_item1 + .@archer_item2 + .@archer_item3 + .@archer_item4;
			set .@total_archer2,(((.@archer_item2 + .@archer_item3) * 2) + .@archer_item4);
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " ดีมาก!";
			mes "เอาล่ะ,";
			mes "ไหนขอดูสิ...";
			next;
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " ข้าจะประเมินมูลค่าชนิดต่างๆของ Trunks, ที่จำเป็นในการสร้าง Bow, ที่เจ้าได้นำมา.";
			next;
			if (countitem(1066) == 0 && countitem(1067) == 0 && countitem(1068) == 0 && countitem(1019) == 0) {
				mes "[เจ้าหน้าที่สมาคม Archer]";
				mes " อืม...";
				mes "น่าเสียดายที่เจ้าไม่ได้นำของที่จำเป็นมา ไม่มีอะไรที่ข้าต้องประเมิน";
				close;
			}
			mes "[เจ้าหน้าที่สมาคม Archer]";
			if (countitem(1066) != 0) mes " เกรด S : " + countitem(1066) + " ชิ้น, คะแนน : " + .@archer_item1 + " . ";
			if (countitem(1067) != 0) mes " เกรด A : " + countitem(1067) + " ชิ้น, คะแนน : " + .@archer_item2 + " . ";
			if (countitem(1068) != 0) mes " เกรด B : " + countitem(1068) + " ชิ้น, คะแนน : " + .@archer_item3 + " . ";
			if (countitem(1019) != 0) mes " เกรด C : " + countitem(1019) + " ชิ้น, คะแนน : " + .@archer_item4 + " . ";
			if (.@total_archer < 25) {
				mes " คะแนนรวม: ^FF0000" + .@total_archer + "^000000 / 40";
				next;
				mes "[เจ้าหน้าที่สมาคม Archer]";
				mes " น้อยกว่า 25! หรอ? เจ้าต้องได้รับคะแนนอย่างน้อย 25! พยายามเข้า!";
				close;
			}
			else {
				mes " คะแนนรวม: ^0000FF" + .@total_archer + "^000000 / 40";
				next;
				mes "[เจ้าหน้าที่สมาคม Archer]";
				if (.@total_archer > 40) {
					mes " ว้าว! มากกว่า 40!";
					mes " ดีมาก! ยินดีด้วย!";
				}
				else if (.@total_archer > 30) {
					mes " มากกว่า 30! เยี่ยมมาก!";
					mes " ยินดีด้วย!";
				}
				else {
					mes " *ถอนหายใจ* ดี, เจ้าเกือบจะไม่ผ่าน... อย่างไรก็ตาม, เจ้าทำได้ดี";
				}
			}
			next;
			mes "[เจ้าหน้าที่สมาคม Archer]";
			mes " ข้าจะให้ช่างฝีมือของสมาคมนำ Trunks พวกนี้ไปทำ Bow ให้. ตอนนี้เจ้าได้ปฏิบัติตามข้อกำหนดแล้ว!";
			if (countitem(1066) != 0) delitem 1066,countitem(1066); //Tree_Of_Archer_1
			if (countitem(1067) != 0) delitem 1067,countitem(1067); //Tree_Of_Archer_2
			if (countitem(1068) != 0) delitem 1068,countitem(1068); //Tree_Of_Archer_3
			if (countitem(1019) != 0) delitem 1019,countitem(1019); //Wooden_Block
		}
		next;
		callfunc "Job_Change",Job_Archer;
		callfunc "F_ClearJobVar";
		completequest 1004;
		mes "[เจ้าหน้าที่สมาคม Archer]";
		mes " ขอแสดงความยินดีด้วย!";
		mes " เจ้าได้เป็น Archer แล้ว!";
		next;
		mes "[เจ้าหน้าที่สมาคม Archer]";
		mes " แน่นอน, เราหวังว่าเจ้าจะช่วยสนับสนุนอนาคตของ สมาคม Archer ด้วยความพยายามของเจ้า";
		next;
		mes "[เจ้าหน้าที่สมาคม Archer]";
		mes " อ่า, ธนูของเจ้าได้มาถึงแล้ว นี้รับไปสิมันเป็นของเจ้า~";
		getitem 1702,1; //Bow_
		getitem 1750,.@total_archer2; //Arrow
		next;
		mes "[เจ้าหน้าที่สมาคม Archer]";
		mes " ตอนนี้ เจ้าจงออกไปล่าด้วยความภาคภูมืใจ!";
		close;
	case 2:
		mes "[เจ้าหน้าที่สมาคม Archer]";
		mes " ข้าจะอธิบายข้อกำหนดสำหรับการเป็น Archer.";
		if (BaseJob != Job_Novice) {
			if (BaseJob == Job_Archer) {
				next;
				mes "[เจ้าหน้าที่สมาคม Archer]";
				mes " แต่...";
				mes " เจ้าเป็น Archer อยู่แล้วนิ เจ้าควรจะรู้อยู่แล้ว...";
			}
			else {
				next;
				mes "[เจ้าหน้าที่สมาคม Archer]";
				mes " เดี๋ยวนะ เจ้าได้เลือกอาชีพไปแล้วนิ เจ้าไม่จำเป็นต้องรู้หรอก~";
			}
			mes " ดั้งนั้น...ใช่...ข้าไม่มีเหตุผลที่จะอธิบายให้เจ้าฟัง...";
		}
		next;
		mes "[เจ้าหน้าที่สมาคม Archer]";
		mes " ขั้นแรกเจ้าต้องฝึกฝนจนเจ้ามี Job Level 9 และเรียน Basic Skills จนครบ";
		next;
		mes "[เจ้าหน้าที่สมาคม Archer]";
		mes " เนื่องจากอาชีพ Archer นั้นเป็นอาชีพที่หลายๆ คนก็ไฝ่ฝัน ข้าจึงต้องการให้พวกเจ้าผ่านบททดสอบนี้ไปก่อน";
		next;
		mes "[เจ้าหน้าที่สมาคม Archer]";
		mes " เจ้าต้องรวบรวมคะแนนจาก ^FF0000Trunks^000000 ที่มีอยู่ 4 ชนิดในธรรมชาติซึ่งมีคุณสมบัติและคุณภาพที่แตกต่างกัน";
		next;
		mes "[เจ้าหน้าที่สมาคม Archer]";
		mes " เพื่อที่เจ้าจะได้เป็น Archer, เจ้าต้องมีคะแนนอย่างน้อย ^0000FF25^000000 คะแนน จนถึง 40 ไม้ทั้งหมดจะหาได้จาก 'Willow' เจ้าควรจะต้องใช้ความระมัดระวัง";
		close;
	case 3:
		close;
	}
}

